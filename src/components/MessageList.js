import React, { useState, useRef, useEffect } from "react";
import Message from "./Message.js";
import OwnMessage from "./OwnMessage.js";
import MessageInput from "./MessageInput.js";
import EditForm from "./EditForm.js";
/*
компонент который содержит список сообщений (.message-list). Список должен быть разделен по дням (.messages-divider) 
(линия с указанным днем, в формате "Today", "Yesterday", "Monday, 17 June")
*/

const MessageList = ({ state, setState }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [currentMessage, setCurrentMessage] = useState({});

  function handleEditInputChange(e) {
    setCurrentMessage({ ...currentMessage, text: e.target.value });
  }

  function handleUpdateMessage(id, updateEvent) {
    const updatedItem = state.map((event) => {
      return event.id === id ? updateEvent : event;
    });
    setIsEditing(false);
    setState(updatedItem);
  }

  function handleEditFormSubmit(e) {
    e.preventDefault();
    handleUpdateMessage(currentMessage.id, currentMessage);
  }

  function handleEditClick(message) {
    setIsEditing(true);
    setCurrentMessage({ ...message, editedAt: new Date() });
  }

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };
  useEffect(scrollToBottom, [state]);

  return (
    <div className="message-list">
      <div className="messages-divider">
        {state.map((message) => {
          <div className="messages-date">{message.createdAt}</div>;
          if (message.user !== "Vladislav") {
            return (
              <Message
                text={message.text}
                avatar={message.avatar}
                id={message.id}
                key={message.id}
                user={message.user}
                time={message.createdAt}
              />
            );
          } else if (message.user === "Vladislav") {
            return (
              <OwnMessage
                text={message.text}
                id={message.id}
                key={message.id}
                user={message.user}
                time={message.createdAt}
                state={state}
                setState={setState}
                item={message}
                onEditClick={handleEditClick}
              />
            );
          }
          return null;
        })}
        <div ref={messagesEndRef} />
      </div>
      {isEditing ? (
        <EditForm
          currentMessage={currentMessage}
          setIsEditing={setIsEditing}
          onEditInputChange={handleEditInputChange}
          onEditFormSubmit={handleEditFormSubmit}
        />
      ) : (
        <MessageInput state={state} setState={setState} />
      )}
    </div>
  );
};


export default MessageList;