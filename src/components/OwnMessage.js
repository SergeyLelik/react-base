import React from "react";
/*
компонент собственного сообщения (.own-message), содержит текст (.message-text), 
время (.message-time) в формате hh:mm, кнопки редактирования (.message-edit) и удаление (.message-delete).
*/

const OwnMessage = ({
    text,
    user,
    time,
    setState,
    state,
    item,
    onEditClick,
}) => {
    const deleteHandler = () => {
        setState(state.filter((ev) => ev.id !== item.id));
    };
    const date = new Date(time);
    const dateLine =
        ("00" + date.getHours()).slice(-2) + ":" + ("00" + date.getMinutes()).slice(-2);

    const monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    const days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];


    const dateEvent = new Date(time);
    const month = monthNames[dateEvent.getMonth()];
    const dayOfWeek = days[dateEvent.getDay()];
    const day = dateEvent.getUTCDate();
    const year = dateEvent.getUTCFullYear();

    return (
        <div>
            <div className="own-messages-date">
                {dateEvent.getDate() === new Date().getDate()
                    ? <h3>Today</h3>
                    : dateEvent.getDate() === Date.now() - 86400000
                        ? <h3>Yesterday</h3>
                        : `${dayOfWeek} ${day}  ${month} ${year}`}
            </div>
            <div className="own-message">
                <p className="message-user-name">{user}</p>
                <p className="message-text">{text}</p>
                <p className="message-time">{dateLine}</p>
                <button onClick={() => onEditClick(item)}>Edit</button>
                <button className="message-delete" onClick={deleteHandler}>Delite</button>
                <p>---------------------------------</p>
            </div>
        </div>
    );
};


export default OwnMessage;