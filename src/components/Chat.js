import { useState, useEffect } from "react";
import Header from "./Header.js";
import MessageList from "./MessageList.js";
import Preloader from "./Preloader.js";

/*
компонент (.chat), который принимает URL ресурс с данными для чата и рисует все перечислены компоненты выше.
*/


function Chat() {
  const [state, setState] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const url = "https://edikdolynskyi.github.io/react_sources/messages.json";
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        setState(json.sort((a, b) => a.createdAt.localeCompare(b.createdAt)));
        setLoading(false);
      } catch (error) {
        console.log("load json error", error);
      }
    };
    fetchData();
  },
    []);


  return (
    <div className="chat">
      <Header state={state} />
      {<Preloader /> && loading}
      <MessageList state={state} setState={setState} />
    </div>
  );
}

export default Chat;