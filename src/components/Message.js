import React from "react";
/*
компонент сообщения (.message), должен содержать текст (.message-text), время (.message-time) в формате hh:mm, 
имя пользователя (.message-user-name) и аватар (.message-user-avatar).
*/

const Message = ({ text, avatar, user, time }) => {

  const date = new Date(time);
  const dateLine =
    ("00" + date.getHours()).slice(-2) + ":" + ("00" + date.getMinutes()).slice(-2);
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const dateEvent = new Date(time);

  const month = monthNames[dateEvent.getMonth()];
  const dayOfWeek = days[dateEvent.getDay()];
  const day = dateEvent.getUTCDate();
  const year = dateEvent.getUTCFullYear();

  if (dateEvent.getDate() === new Date().getDate()) {
    const newdate = `${dayOfWeek} ${day}, ${month} ${year}`;
    return newdate;
  }
  const newdate = `${dayOfWeek} ${day}, ${month} ${year}`;

  return (
    <div>
      <div className="messages-date">{newdate}</div>
      <div className={`message ${user === "Vladislav" ? "own-message" : ""}`}>
        <img src={avatar} alt="avatar" />
        <p className="message-user-name">{user}</p>
        <p className="message-text">{text}</p>
        <p className="message-time">{dateLine}</p>
        <p>---------------------------------</p>
      </div>
    </div>
  );
};


export default Message;