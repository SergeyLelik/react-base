import React, { useState } from "react";
/*
компонент (.message-input), который содержит текстовое поле (.message-input-text) и кнопку "Send" (.message-input-button). 
Должен использоваться для отправки и редактирования сообщения.
*/

const MessageInput = ({ state, setState }) => {
   const [input, setInput] = useState("");
  const newMessageHandler = (event) => {
    event.preventDefault();
    if (input.trim().length > 1) {
      setState([
        ...state,
        {
          id: Date.now(),
          user: "Vladislav",
          userId: "Vladislav",
          createdAt: Date.now(),
          editedAt: "",
          text: input,
          
        },
      ]);
      setInput("");
    }
  };


  const handleKeyPress = (enter) => {
    if (enter.key === "Enter") {
      newMessageHandler(enter);
    }
  };
  return (
    <div className="message-input">
      <input
        type="text"
        className="message-input-text"
        placeholder="new message"
        value={input}
        onChange={(input) => {
          setInput(input.target.value);
        }}
        onKeyDown={handleKeyPress}
      />
      <button onClick={newMessageHandler} className="message-input-button">
        Send
      </button>
    </div>
  );
};

export default MessageInput;