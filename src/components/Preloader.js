import React from "react";
/*
компонент со спинером (лоадером) во время загрузки данных (.preloader)

inp any spiner!!!! https://icon-library.com/icon/spinner-icon-gif-10.html
*/

const Preloader = () => {
    return (
        <div id="root">
            <div id="preloader">
                <p>Loading chat...</p>
                <img src="/images/loader.gif" />
            </div>

        </div>
    );
};

export default Preloader;