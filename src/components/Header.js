import React from "react";
/*
отдельный компонент (.header), который содержит название чате (.header-title), количество пользователей (.header-users-count), 
количество сообщений (.header-messages-count), дату последнего сообщения (.header-last-message -date) в формате dd.mm.yyyy hh: mm:ss 
(например: 15.02.2021 13:35)
*/

const Header = (state) => {
  const usersCount = state.state.reduce(function (acc, curr) {
    let isElemExist = acc.findIndex(function (item) {
      return item.user === curr.user;
    });
    if (isElemExist === -1) {
      let obj = {};
      obj.user = curr.user;
      acc.push(obj);
    } else {
      void 0;
    }
    return acc;
  }, []);

  const latestMessage = state.state[state.state.length - 1]?.createdAt;
  const date = new Date(latestMessage);
  const dateStr =
    ("00" + (date.getMonth() + 1)).slice(-2) +
    "." +
    ("00" + date.getDate()).slice(-2) +
    "." +
    date.getFullYear() +
    " " +
    ("00" + date.getHours()).slice(-2) +
    "." +
    ("00" + date.getMinutes()).slice(-2);

  return (
    <div className="header">
      <h2 className="header-title">It's logo chat</h2>
      <div className="header-container">
        <h3 className="header-users-count">
          {usersCount.length} <span>participans</span>
        </h3>
        <h3 className="header-messages-count">
          {state.state.length} <span>messages</span>
        </h3>
        <h3 className="header-last-message-date">
          <span>last messages at </span> {state.state.length > 0 && dateStr}
        </h3>
      </div>
    </div>
  );
};

export default Header;